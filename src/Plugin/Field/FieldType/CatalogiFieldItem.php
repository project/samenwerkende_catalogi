<?php

namespace Drupal\samenwerkende_catalogi\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'catalogi' field type.
 *
 * @FieldType(
 *   id = "catalogi",
 *   label = @Translation("Samenwerkende Catalogi"),
 *   description = @Translation("This field stores xml Samenwerkende Catalogi."),
 *   default_widget = "catalogi_widget",
 *   default_formatter = "catalogi_formatter",
 *   constraints = {"CatalogiConstraint" = {}}
 * )
 */
class CatalogiFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'status' => [
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
          'default' => 0,
        ],
        'data' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['status'] = DataDefinition::create('boolean')
      ->setLabel(t('Status'));
    $properties['data'] = DataDefinition::create('catalogi')
      ->setLabel(t('Data'));

    return $properties;
  }

}
