<?php

namespace Drupal\samenwerkende_catalogi\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'catalogi_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "catalogi_formatter",
 *   module = "samenwerkende_catalogi",
 *   label = @Translation("Empty formatter"),
 *   field_types = {
 *     "catalogi"
 *   }
 * )
 */
class CatalogiFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }

}
