<?php

namespace Drupal\samenwerkende_catalogi\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\samenwerkende_catalogi\Service\SamenwerkendeCatalogiResourceService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Widget for Samenwerkende Catalogi field.
 *
 * @FieldWidget(
 *   id = "catalogi_widget",
 *   label = @Translation("Advanced Samenwerkende Catalogi form"),
 *   field_types = {
 *     "catalogi"
 *   }
 * )
 */
class CatalogiFieldWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\samenwerkende_catalogi\Service\SamenwerkendeCatalogiResourceService
   */
  protected $samenwerkendeCatalogiResource;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, SamenwerkendeCatalogiResourceService $samenwerkende_catalogi_resource,  AccountInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->samenwerkendeCatalogiResource = $samenwerkende_catalogi_resource;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('samenwerkende_catalogi_resource'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#group'] = 'advanced';
    $element['#access'] = $this->currentUser->hasPermission('edit samenwerkende catalogi content');
    $element += [
      '#type' => 'details',
    ];
    
    /** @var \Drupal\samenwerkende_catalogi\Plugin\Field\FieldType\CatalogiFieldItem $item */
    $item = $items[$delta];
    
    $element['#open'] = $item->status;

    $element['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add to Samenwerkende Catalogi'),
      '#default_value' => $item->status,
    ];

    $parents = $element['#field_parents'];
    $parents[] = $this->fieldDefinition->getName();
    $selector = $root = array_shift($parents);
    if ($parents) {
      $selector = $root . '[' . implode('][', $parents) . ']';
    }

    $element['data'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['data']['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [$items->getEntity()->getEntityTypeId()],
      '#global_types' => TRUE,
      '#show_nested' => FALSE,
    ];

    $item_default = unserialize($item->data, ['allowed_classes' => FALSE]);

    $element['data']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $item_default['title'] ?? '',
      '#states' => [
        'required' => [
          ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['data']['authority'] = [
      '#type' => 'select',
      '#title' => $this->t('Authority'),
      '#options' => $this->samenwerkendeCatalogiResource->getOptionsBySamenwerkendeCatalogiName('samenwerkende_catalogi_authority'),
      '#default_value' => $item_default['authority'] ?? NULL,
      '#states' => [
        'required' => [
          ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['data']['spatial'] = [
      '#type' => 'select',
      '#title' => $this->t('Spatial'),
      '#options' => $this->samenwerkendeCatalogiResource->getOptionsBySamenwerkendeCatalogiName('samenwerkende_catalogi_spatial'),
      '#default_value' => $item_default['spatial'] ?? NULL,
      '#states' => [
        'required' => [
          ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['data']['audience'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Audience'),
      '#options' => [
        'particulier' => $this->t('Consumer'),
        'ondernemer' => $this->t('Entrepreneur'),
      ],
      '#multiple' => TRUE,
      '#default_value' => $item_default['audience'] ?? [],
      '#states' => [
        'required' => [
          ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['data']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subjects'),
      '#description' => $this->t('The findability of the product, such as keywords or theme layouts. Separate by comma or use a token.'),
      '#default_value' => $item_default['subject'] ?? ''
    ];

    $element['data']['abstract'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Abstract'),
      '#description' => $this->t('Short description of the product, for presentation in the search results list of buyers. A length of approximately 300 characters is recommended. Plain text or token.'),
      '#default_value' => $item_default['abstract'] ?? '',
      '#states' => [
        'required' => [
          ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['data']['onlineAanvragen'] = [
      '#type' => 'radios',
      '#title' => $this->t('Apply online'),
      '#description' => $this->t('Indicates whether the product can be requested online, and if so whether DigiD is used for this. If not applicable, enter \'no\' here.'),
      '#options' => [
        'ja' => $this->t('Yes'),
        'nee' => $this->t('No'),
        'digid' => $this->t('DigiD'),
      ],
      '#default_value' => $item_default['onlineAanvragen'] ?? NULL,
      '#states' => [
        'required' => [
          ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['data']['aanvraagURL'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Request URL'),
      '#description' => $this->t('Indicates where the product can be requested online, for example an electronic form. Plain text or token.'),
      '#default_value' => $item_default['aanvraagURL'] ?? '',
      '#states' => [
        'visible' => [
          [':input[name="' . $selector . '[0][data][onlineAanvragen]"]' => ['value' => 'ja']],
          [':input[name="' . $selector . '[0][data][onlineAanvragen]"]' => ['value' => 'digid']],
        ],
        'required' => [
          [
            ':input[name="' . $selector . '[0][data][onlineAanvragen]"]' => ['value' => 'ja'],
            ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
          ],
          [
            ':input[name="' . $selector . '[0][data][onlineAanvragen]"]' => ['value' => 'digid'],
            ':input[name="' . $selector . '[0][status]"]' => ['checked' => TRUE],
          ],
        ],
      ],
    ];

    $element['data']['uniformeProductnaam'] = [
      '#type' => 'select',
      '#title' => $this->t('Uniform product name'),
      '#options' => $this->samenwerkendeCatalogiResource->getOptionsBySamenwerkendeCatalogiName('samenwerkende_catalogi_upl'),
      '#multiple' => TRUE,
      '#default_value' => $item_default['uniformeProductnaam'] ?? [],
    ];

    $element['data']['gerelateerdProduct'] = [
      '#type' => 'select',
      '#title' => $this->t('Uniform product name of related products'),
      '#options' => $this->samenwerkendeCatalogiResource->getOptionsBySamenwerkendeCatalogiName('samenwerkende_catalogi_upl'),
      '#multiple' => TRUE,
      '#default_value' => $item_default['gerelateerdProduct'] ?? []
    ];

    $element['data']['productHTML'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Product HTML'),
      '#description' => $this->t('Contains a fragment of formatted XHTML. When filling this field, keep in mind that the XHTML is valid and should be able to be presented as part of any (government) website.'),
      '#default_value' => $item_default['productHTML'] ?? ''
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $id => $value) {
      foreach ($value as $key => $group) {
        if (is_array($group)) {
          $values[$id][$key] = serialize($group);
        }
      }
    }

    return $values;
  }

}
