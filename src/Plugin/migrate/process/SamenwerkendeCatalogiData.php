<?php

namespace Drupal\samenwerkende_catalogi\Plugin\migrate\process;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process plugin for converting data to Samenwerkende Catalogi database format.
 *
 * @MigrateProcessPlugin(
 *   id = "samenwerkende_catalogi_data"
 * )
 */
class SamenwerkendeCatalogiData extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleConfiguration;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ImmutableConfig $module_configuration
   *   The configuration settings for this module.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $module_configuration) {
    $configuration += [
      'sources' => [],
    ];
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleConfiguration = $module_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('samenwerkende_catalogi.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $new_row = new Row($row->getSource());
    $migrate_executable->processRow($new_row, $this->configuration['sources']);
    $data = $this->normalizeData($new_row->getDestination());

    return serialize($data);
  }

  /**
   * Normalizes processed data so it includes default values.
   *
   * @param array $data
   *
   * @return array
   */
  protected function normalizeData(array $data) {
    $defaults = $this->moduleConfiguration->get('data');
    $normalized = array_merge($defaults, $data);

    // Normalize array values.
    $fields = ['audience', 'uniformeProductnaam', 'gerelateerdProduct'];
    foreach ($fields as $field) {
      $field_values = [];
      if (isset($normalized[$field]) && is_array($normalized[$field])) {
        foreach ($normalized[$field] as $field_value) {
          if (!empty($field_value)) {
            $field_values[$field_value] = $field_value;
          }
        }
      }
      $normalized[$field] = $field_values;
    }

    return $normalized;
  }

}
