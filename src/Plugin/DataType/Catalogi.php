<?php

namespace Drupal\samenwerkende_catalogi\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * The Samenwerkende Catalogi data type.
 *
 * @DataType(
 *  id = "catalogi",
 *  label = @Translation("Samenwerkende Catalogi")
 * )
 */
class Catalogi extends StringData implements CatalogiInterface {

}
