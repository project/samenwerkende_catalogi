<?php

namespace Drupal\samenwerkende_catalogi\Plugin\DataType;

use Drupal\Core\TypedData\PrimitiveInterface;

/**
 * The catalogi data type.
 */
interface CatalogiInterface extends PrimitiveInterface {

}
