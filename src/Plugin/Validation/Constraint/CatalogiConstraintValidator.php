<?php

namespace Drupal\samenwerkende_catalogi\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class CatalogiConstraintValidator.
 *
 * @package Drupal\samenwerkende_catalogi\Plugin\Validation\Constraint
 */
class CatalogiConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {

    /** @var \Drupal\Core\TypedData\Plugin\DataType\BooleanData $status */
    $status = $items->get('status');

    // If there is no value or is 0 we don't need to validate anything.
    if (!$status->getValue()) {
      return NULL;
    }

    $required_fields = [
      'title', 'spatial', 'authority', 'audience', 'abstract', 'onlineAanvragen',
    ];

    $required_field_not_set = NULL;

    $data = $data = unserialize($items->get('data')->getValue(), ['allowed_classes' => FALSE]);
    foreach ($required_fields as $required_field) {
      if (isset($data[$required_field])) {
        $field_data = $data[$required_field];
        if (is_array($field_data) && count(array_unique($field_data)) === 1 && end($field_data) === 0) {
          $required_field_not_set = TRUE;
          break;
        }
        if (empty($field_data)) {
          $required_field_not_set = TRUE;
          break;
        }
        if ($required_field == 'onlineAanvragen' && in_array($data[$required_field], ['digid', 'ja']) && empty($data['aanvraagURL'])) {
          $required_field_not_set = TRUE;
          break;
        }
      }
    }

    if ($required_field_not_set) {
      $this->context->addViolation($constraint->requiredFields, [
        '%field_name' => $required_field,
      ]);
    }

  }

}
