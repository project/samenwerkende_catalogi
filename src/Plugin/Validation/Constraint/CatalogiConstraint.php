<?php

namespace Drupal\samenwerkende_catalogi\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the catalogi fields are set when it is activated.
 *
 * @Constraint(
 *   id = "CatalogiConstraint",
 *   label = @Translation("Samenwerkende catalogi", context = "Validation"),
 * )
 */
class CatalogiConstraint extends Constraint {

  public $requiredFields = 'Samenwerkende Catalogi field %field_name can not be empty.';

}
