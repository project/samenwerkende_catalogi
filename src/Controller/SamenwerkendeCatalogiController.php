<?php

namespace Drupal\samenwerkende_catalogi\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\samenwerkende_catalogi\Service\SamenwerkendeCatalogiXmlService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SamenwerkendeCatalogiController.
 *
 * @package Drupal\samenwerkende_catalogi\Controlle
 */
class SamenwerkendeCatalogiController extends ControllerBase {

  /**
   * @var \Drupal\samenwerkende_catalogi\Service\SamenwerkendeCatalogiXmlService
   */
  protected $samenwerkendeCatalogiXmlService;

  /**
   * SamenwerkendeCatalogiController constructor.
   *
   * @param \Drupal\samenwerkende_catalogi\Service\SamenwerkendeCatalogiXmlService $samenwerkende_catalogi_xml_services
   */
  public function __construct(SamenwerkendeCatalogiXmlService $samenwerkende_catalogi_xml_services) {
    $this->samenwerkendeCatalogiXmlService = $samenwerkende_catalogi_xml_services;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('samenwerkende_catalogi_xml')
    );
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function render() {
    $response = new CacheableResponse();
    $response->addCacheableDependency($this->samenwerkendeCatalogiXmlService->xml());
    $response->getCacheableMetadata()->addCacheTags(['node_list']);
    $response->setContent($this->samenwerkendeCatalogiXmlService->xml());
    $response->headers->set('Content-Type', 'text/xml');

    return $response;
  }

}
