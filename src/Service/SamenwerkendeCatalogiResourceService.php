<?php

namespace Drupal\samenwerkende_catalogi\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class SamenwerkendeCatalogiResourceService.
 *
 * @package Drupal\samenwerkende_catalogi\Service
 */
class SamenwerkendeCatalogiResourceService {

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * SamenwerkendeCatalogiResourceService constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $this->cacheBackend = $cache_backend;
    $this->moduleHandler = $module_handler;
  }

  /**
   * @param string $name
   *
   * @return array
   */
  public function getOptionsBySamenwerkendeCatalogiName(string $name) {
    $options = [];
    if ($cache = $this->cacheBackend->get($name)) {
      $options = $cache->data;
    }
    elseif ($samenwerkende_catalogi_files = $this->getSamenwerkendeCatalogiResourcesByName($name)) {
      foreach ($samenwerkende_catalogi_files as $xml_file) {
        $xml = simplexml_load_file($xml_file);
        $scheme = ($name === 'samenwerkende_catalogi_upl') ? '' : strip_tags($xml->attributes()->name);
        $options = array_merge($options, $this->generateOptionsByXmlFile($xml_file, $scheme));
      }
      asort($options, SORT_STRING);
      $this->cacheBackend->set($name, $options, $expire = Cache::PERMANENT, ['samenwerkende_catalogi']);
    }

    return $options;
  }

  /**
   * @param string $name
   *
   * @return array
   */
  protected function getSamenwerkendeCatalogiResourcesByName(string $name) {
    $module_path = $this->moduleHandler->getModule('samenwerkende_catalogi')->getPath();
    $directory = $module_path . '/resources/';
    switch ($name) {
      case  'samenwerkende_catalogi_authority':
        $samenwerkende_catalogi_files = preg_grep(
          '/(?=.*UniformeProductnaam)|(?=.*Koninkrijksdeel).*/',
          glob($directory . "/*.xml"), PREG_GREP_INVERT
        );
        break;

      case 'samenwerkende_catalogi_spatial':
        $samenwerkende_catalogi_files = preg_grep(
          '/(?=.*UniformeProductnaam)|(?=.*AndereOrganisatie)|(?=.*Ministerie).*/',
          glob($directory . "/*.xml"), PREG_GREP_INVERT
        );
        break;

      case 'samenwerkende_catalogi_upl':
        $samenwerkende_catalogi_files = glob($directory . "/UniformeProductnaam.xml");
        break;

      default:
        $samenwerkende_catalogi_files = [];
    }

    return $samenwerkende_catalogi_files;
  }

  /**
   * @param string $xml_file
   * @param string $scheme
   *
   * @return array
   */
  protected function generateOptionsByXmlFile(string $xml_file, string $scheme) : array {
    $xml = simplexml_load_file($xml_file);
    $options = [];
    /** @var \SimpleXMLElement $row */
    foreach ($xml as $row) {
      if (!empty($scheme)) {
        $options[$scheme . '|' . strip_tags($row->resourceIdentifier->asXML())] = strip_tags($row->prefLabel->asXML());
      }
      else {
        $options[strip_tags($row->resourceIdentifier->asXML())] = strip_tags($row->prefLabel->asXML());
      }
    }
    return $options;
  }

}
