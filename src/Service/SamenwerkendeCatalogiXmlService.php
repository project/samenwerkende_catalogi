<?php

namespace Drupal\samenwerkende_catalogi\Service;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Utility\Token;
use Drupal\samenwerkende_catalogi\Plugin\Field\FieldType\CatalogiFieldItem;

/**
 * Class SamenwerkendeCatalogiXmlService.
 *
 * @package Drupal\samenwerkende_catalogi\Service
 */
class SamenwerkendeCatalogiXmlService {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The date service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $samenwerkendeCatalogiResource;

  /**
   * SamenwerkendeCatalogiXmlService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\Core\Utility\Token $token
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   * @param \Drupal\samenwerkende_catalogi\Service\SamenwerkendeCatalogiResourceService $samenwerkende_catalogi_resource
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, Token $token, DateFormatter $date_formatter, SamenwerkendeCatalogiResourceService $samenwerkende_catalogi_resource) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->token = $token;
    $this->dateFormatter = $date_formatter;
    $this->samenwerkendeCatalogiResource = $samenwerkende_catalogi_resource;
  }

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function xml() {
    return $this->buildDocument()->saveXML();
  }

  /**
   * @return \DOMDocument
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function buildDocument() {
    $doc = new \DOMDocument('1.0', 'UTF-8');
    $root = $doc->createElementNS('http://standaarden.overheid.nl/product/terms/', 'overheidproduct:scproducten');
    $root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:overheid', 'http://standaarden.overheid.nl/owms/terms/');
    $root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:dcterms', 'http://purl.org/dc/terms/');
    $root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    $root->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation', 'http://standaarden.overheid.nl/product/terms/ http://standaarden.overheid.nl/sc/4.0/xsd/sc.xsd');

    $catalogi_fields_map = $this->entityFieldManager->getFieldMapByFieldType('catalogi');
    foreach ($catalogi_fields_map as $entity_type => $fields) {
      foreach ($fields as $field_name => $field_data) {
        if (!empty($field_data['bundles'])) {
          foreach ($field_data['bundles'] as $bundle) {
            $entities = $this->entityTypeManager->getStorage($entity_type)
              ->loadByProperties([
                'type' => $bundle,
                'status' => 1,
                "{$field_name}.status" => 1,
              ]);
            if (!empty($entities)) {
              /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
              foreach ($entities as $entity) {
                /** @var \Drupal\samenwerkende_catalogi\Plugin\Field\FieldType\CatalogiFieldItem $item */
                foreach ($entity->{$field_name} as $item) {
                  $data = $this->getDataByItemAndEntity($item, $entity);
                  $product_element = $this->generateProductElement($doc, $data);
                  $root->appendChild($product_element);
                }
              }
            }
          }
        }
      }
    }
    $doc->appendChild($root);

    return $doc;
  }

  /**
   * @param \Drupal\samenwerkende_catalogi\Plugin\Field\FieldType\CatalogiFieldItem $item
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return mixed
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getDataByItemAndEntity(CatalogiFieldItem $item, ContentEntityInterface $entity) {
    $data = unserialize($item->data, ['allowed_classes' => FALSE]);
    $data['modified'] = $this->dateFormatter->format(
      $entity->get('changed')->value,
      'custom',
      'Y-m-d'
    );
    $data['type'] = 'productbeschrijving';
    $data['language'] = $entity->language()->getId();
    $url = $entity->toUrl('canonical', ['absolute' => TRUE])->toString(TRUE);
    $data['identifier'] = $url->getGeneratedUrl();

    $data['productID'] = $entity->id();

    if (!empty($data['audience'])) {
      foreach ($data['audience'] as $id => $audience) {
        if (is_int($audience)) {
          unset($data['audience'][$id]);
        }
      }
    }

    foreach ($data as $key => $item) {
      if (is_string($item)) {
        $data[$key] = $this->token->replace(
          $item,
          [$entity->getEntityTypeId() => $entity],
          ['clear' => TRUE],
          new BubbleableMetadata()
        );
      }
    }

    return $data;
  }

  /**
   * @param \DOMDocument $doc
   * @param array $data
   *
   * @return \DOMElement
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function generateProductElement(\DOMDocument $doc, array $data) {
    // Product -> Meta.
    $meta = $doc->createElement('overheidproduct:meta');

    // Product -> Meta -> Kern.
    $kern = $doc->createElement('overheidproduct:owmskern');
    foreach ($this->generateOwmskernElements($doc, $data) as $owmskern_element) {
      $kern->appendChild($owmskern_element);
    }
    $meta->appendChild($kern);

    // Product -> Meta -> Mantel.
    $mantel = $doc->createElement('overheidproduct:owmsmantel');
    foreach ($this->generateOwmsmantelElements($doc, $data) as $owmsmantel_element) {
      $mantel->appendChild($owmsmantel_element);
    }
    $meta->appendChild($mantel);

    // Product -> Meta -> Scmeta.
    $scmeta = $doc->createElement('overheidproduct:scmeta');
    foreach ($this->generateScmetaElements($doc, $data) as $scmeta_element) {
      $scmeta->appendChild($scmeta_element);
    }
    $meta->appendChild($scmeta);

    // Product.
    $product_element = $doc->createElement('overheidproduct:scproduct');
    $product_element->setAttribute('owms-version', '4.0');
    $product_element->appendChild($meta);

    // Product -> Body.
    $product_body = $doc->createElement('overheidproduct:body');
    $productHtml = $doc->createElement('overheidproduct:productHTML');
    $productHtml->setAttribute('xmlns', 'http://www.w3.org/1999/xhtml');
    $productHtml->setAttribute('xsi:schemaLocation', 'http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd');
    $productHtml->appendChild($doc->createCDATASection($data['productHTML']));
    $product_body->appendChild($productHtml);
    $product_element->appendChild($product_body);

    return $product_element;
  }

  /**
   * @param \DOMDocument $doc
   * @param array $data
   *
   * @return array
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function generateOwmskernElements(\DOMDocument $doc, array $data) {
    $owmskern = [
      'identifier' => ['CDATA'],
      'title' => ['CDATA'],
      'language' => ['nodeValue'],
      'type' => [
        'nodeValue' => 'nodeValue',
        'attributes' => [
          'scheme' => 'overheid:Informatietype',
        ],
      ],
      'modified' => ['nodeValue'],
    ];
    if (isset($data['spatial'])) {
      $spatial_options = $this->samenwerkendeCatalogiResource->getOptionsBySamenwerkendeCatalogiName('samenwerkende_catalogi_spatial');
      $spatial_option = $data['spatial'];
      $data['spatial'] = $spatial_options[$spatial_option];
      $spatial_option_pieces = explode('|', $spatial_option);
      $owmskern['spatial'] = [
        'nodeValue' => 'nodeValue',
        'attributes' => [
          'scheme' => $spatial_option_pieces[0],
          'resourceIdentifier' => $spatial_option_pieces[1],
        ],
      ];
    }

    $owmskern_elements = $this->createElements($owmskern, $doc, 'dcterms', $data);

    if (isset($data['authority'])) {
      $authority_options = $this->samenwerkendeCatalogiResource->getOptionsBySamenwerkendeCatalogiName('samenwerkende_catalogi_authority');
      $authority_option = $data['authority'];
      $data['authority'] = $authority_options[$authority_option];
      $authority_option_pieces = explode('|', $authority_option);
      $owmskern_authority['authority'] = [
        'nodeValue' => 'nodeValue',
        'attributes' => [
          'scheme' => $authority_option_pieces[0],
          'resourceIdentifier' => $authority_option_pieces[1],
        ],
      ];
      $owmskern_elements = array_merge($owmskern_elements, $this->createElements($owmskern_authority, $doc, 'overheid', $data));
    }

    return $owmskern_elements;
  }

  /**
   * @param \DOMDocument $doc
   * @param array $data
   *
   * @return array
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function generateOwmsmantelElements(\DOMDocument $doc, array $data) {
    $owmsmantel = [
      'audience' => [
        'nodeValue' => 'nodeValue',
        'attributes' => [
          'scheme' => 'overheid:Doelgroep',
        ],
      ],
      'subject' => ['CDATA'],
      'abstract' => ['strip_tags', 'CDATA'],
    ];
    if (isset($data['subject'])) {
      $owmsmantel['subject'] = ['CDATA'];
      $data['subject'] = explode(',', $data['subject']);
    }

    $owmsmantel_elements = $this->createElements($owmsmantel, $doc, 'dcterms', $data);

    return $owmsmantel_elements;
  }

  /**
   * @param \DOMDocument $doc
   * @param array $data
   *
   * @return array
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function generateScmetaElements(\DOMDocument $doc, array $data) {
    $scmeta = [
      'productID' => ['nodeValue'],
      'onlineAanvragen' => ['nodeValue'],
    ];
    if (!empty($data['aanvraagURL'])) {
      $scmeta['aanvraagURL'] = [
        'attributes' => [
          'resourceIdentifier' => $data['aanvraagURL'],
        ],
      ];
    }

    $scmeta_elements = $this->createElements($scmeta, $doc, 'overheidproduct', $data);

    if (isset($data['uniformeProductnaam']) && !empty($data['uniformeProductnaam'])) {
      $upl_options = $this->samenwerkendeCatalogiResource->getOptionsBySamenwerkendeCatalogiName('samenwerkende_catalogi_upl');
      foreach ($data['uniformeProductnaam'] as $uniforme_productnaam) {
        $uniformeProductnaam_element = $doc->createElement("overheidproduct:uniformeProductnaam");
        $upl_option = $uniforme_productnaam;
        $uniformeProductnaam_element->nodeValue = $upl_options[$upl_option];
        $uniformeProductnaam_element->setAttribute('scheme', 'overheid:UniformeProductnaam');
        $uniformeProductnaam_element->setAttribute('resourceIdentifier', $upl_option);
        $scmeta_elements[] = $uniformeProductnaam_element;
      }
    }

    if (isset($data['gerelateerdProduct']) && !empty($data['gerelateerdProduct'])) {
      $upl_options = $this->samenwerkendeCatalogiResource->getOptionsBySamenwerkendeCatalogiName('samenwerkende_catalogi_upl');
      foreach ($data['gerelateerdProduct'] as $gerelateerd_product) {
        $gerelateerdProduct_element = $doc->createElement("overheidproduct:gerelateerdProduct");
        $upl_option = $gerelateerd_product;
        $gerelateerdProduct_element->nodeValue = $upl_options[$upl_option];
        $gerelateerdProduct_element->setAttribute('scheme', 'overheid:UniformeProductnaam');
        $gerelateerdProduct_element->setAttribute('resourceIdentifier', $upl_option);
        $scmeta_elements[] = $gerelateerdProduct_element;
      }

    }

    return $scmeta_elements;
  }

  /**
   * @param array $meta
   * @param \DOMDocument $doc
   * @param string $id_prefix
   * @param array $data
   * @param array $elements
   *
   * @return array
   */
  protected function createElements(array $meta, \DOMDocument $doc, string $id_prefix, array $data, array $elements = []) {
    foreach ($meta as $id => $settings) {
      if (!empty($data[$id])) {
        if (is_array($data[$id])) {
          foreach ($data[$id] as $item) {
            $elements[] = $this->createElement($doc, $id_prefix, $id, $item, $settings);
          }
        }
        else {
          $elements[] = $this->createElement($doc, $id_prefix, $id, $data[$id], $settings);
        }
      }
    }

    return $elements;
  }

  /**
   * @param \DOMDocument $doc
   * @param string $id_prefix
   * @param string $id
   * @param string $item
   * @param mixed $settings
   *
   * @return \DOMElement
   */
  protected function createElement(\DOMDocument $doc, string $id_prefix, string $id, string $item, $settings) {
    $element = $doc->createElement("{$id_prefix}:{$id}");
    if ($settings && in_array('strip_tags', $settings)) {
      $item = strip_tags($item);
    }
    if ($settings && in_array('CDATA', $settings)) {
      $element->appendChild($doc->createCDATASection($item));
    }
    elseif ($settings && in_array('nodeValue', $settings)) {
      $element->nodeValue = $item;
    }
    if ($settings && !empty($settings['attributes'])) {
      foreach ($settings['attributes'] as $id => $value) {
        $element->setAttribute($id, $value);
      }
    }

    return $element;
  }

}
