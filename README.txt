Drupal Samenwerkende Catalogi module:
----------------------
Author - see https://drupal.org/project/samenwerkende_catalogi
Requires - Drupal 8 || 9
License - GPL (see LICENSE)


Overview:
--------
The Samenwerkende Catalogi module creates a XML feed for the
Samenwerkende Catalogi index by the data of nodes that should be
included to the XML feed.

The Samenwerkende Catalogi service of the Dutch central government
links products and services from different government organisations
with the purpose of finding information easier for entrepreneurs and
citizens.

Used xml resources for Samenwerkende Catalogi:
- AndereOrganisatie.xml: https://standaarden.overheid.nl/owms/4.0/doc/waardelijsten/overheid.andereorganisatie
- Gemeente.xml: https://standaarden.overheid.nl/owms/4.0/doc/waardelijsten/overheid.gemeente
- GGD.xml: https://standaarden.overheid.nl/owms/4.0/doc/waardelijsten/overheid.ggd
- Koninkrijksdeel.xml: https://standaarden.overheid.nl/owms/4.0/doc/waardelijsten/overheid.koninkrijksdeel
- Ministerie.xml: https://standaarden.overheid.nl/owms/4.0/doc/waardelijsten/overheid.ministerie
- Provincie.xml: https://standaarden.overheid.nl/owms/4.0/doc/waardelijsten/overheid.provincie
- UniformeProductnaam.xml: https://standaarden.overheid.nl/owms/4.0/doc/waardelijsten/overheid.uniformeproductnaam
- Waterschap.xml: https://standaarden.overheid.nl/owms/4.0/doc/waardelijsten/overheid.waterschap

For more information and documentation about samenwerkende catalogi: https://www.logius.nl/diensten/samenwerkende-catalogi


Composer Installation:
------------
1. composer require drupal/samenwerkende_catalogi

Basis Installation:
------------
1. Place this module directory in your modules folder (this will
   usually be "modules/" for Drupal 8/9).
2. Go to Manage -> Extend to enable the module.

Configuration:
------------
1. Add new Samenwerkende Catalogi field to your content type.
2. Visit http://example.com/sc-feed.